.PHONY: help
help:  ## Show this help
	@echo 'makepac'
	@echo '--------'
	@echo 'Simple configuration manager'
	@echo ''
	@echo 'See https://gitlab.com/balki/makepac for usage'
	@echo ''
	@#https://stackoverflow.com/a/47107132
	@sed -ne '/@sed/!s/## //p' $(MAKEFILE_LIST) | column -tl 2

.PHONY: install
install:  ## Installs files in root/ to real root
	sudo ./makepac/install.sh

.PHONY: sd-reload
sd-reload:  ## Run this after editing systemd service/timer files
	sudo systemctl daemon-reload

.PHONY: sd-users
sd-users:  ## Run this after adding new users
	sudo systemctl restart systemd-sysusers.service 

.PHONY: save-perms
save-perms:  ## Saves the file permissions to ./acl. Run this after adding new files with correct permissions
	getfacl --recursive root > acl

.PHONY: set-perms
set-perms:  ## Restores permissions of all files from ./acl
	sudo setfacl --restore acl

.PHONY: skel
skel:  ## Creates common directories needed
	mkdir -p root/etc/
	mkdir -p root/usr/local/bin
	mkdir -p root/usr/local/lib/systemd/system
	mkdir -p root/usr/local/lib/sysusers.d
