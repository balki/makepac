`makepac` is a simple configuration manager for a linux system

A typical linux app needs multiple files in different paths spread in the file-system. E.g. systemd service, timer, app config, webserver config, logrotate config. `makepac` keeps all configs in a single place in a git repository and copies them to real root

Easily share the setup to other user.

Initial setup
-------------

    mkdir myfiles
    cd myfiles
    git init
    git submodule add https://gitlab.com/balki/makepac'
    echo "include makepac/Makefile" > Makefile'
    sudo apt install acl
    make skel
    git add .
    git commit -m 'initial commit'


Workflow
--------

1. Create required files under root/
2. Set correct ownership and permissions
3. Run `make intall`
4. Run `make sd-users` and `make sd-reload` if required
5. Enable and test the service and update all configs as required
6. Run `make save-perms` to update `./acl` file
7. Add any important notes in README (like dependencies, additional commands ran)
8. Finally `git commit`

Cloning from other user
-----------------------

    git clone <other user repo url>
    make set-perms
    make install

Usage
-----

    ❯ make help
    makepac
    --------
    Simple configuration manager

    See https://gitlab.com/balki/makepac for usage

    help:         Show this help
    install:      Installs files in root/ to real root
    sd-reload:    Run this after editing systemd service/timer files
    sd-users:     Run this after adding new users
    save-perms:   Saves the file permissions to ./acl. Run this after adding new files with correct permissions
    set-perms:    Restores permissions of all files from ./acl
    skel:         Creates common directories needed

Tips
----
1. When adding a file with no public read permissions, it can't be saved in the git repo. You can git-ignore the file or add read permission for the repo use alone using this command. Also consider setting up [git-crypt](https://www.agwa.name/projects/git-crypt/) for those files

    sudo setfacl -m "u:$USER:r" root/etc/app/secret_token

