#!/usr/bin/bash

root="root"

all_files() {
		find $root -type f,l
}

for f in $(all_files)
do
		install_path="${f#"$root"}"
		mkdir -p "$(dirname "$install_path")"

		# Sometimes it is simpler to just link instead of copy
		# ln -snf "$PWD/$f" "$install_path"

		# install -C only copies if destination has changed. 
		# However for symlinks, it always copies and it copies the *contents* and not the symlinks. 
		if [[ -L "$PWD/$f" ]]; then
				cp -P "$PWD/$f" "$install_path"
		else
				read -ra perm_options <  <(stat -c '-m %a -g %g -o %u' "$PWD/$f")
				install -C "${perm_options[@]}" "$PWD/$f" "$install_path"
		fi
done

# vim: set filetype=bash:
